# Pipeline de Dados - Observatório de Resíduos Sólidos

Este repositório contém scripts e recursos para construir e manter o pipeline de dados para extração, transformação e carregamento (ETL) dos sistemas responsáveis pela limpeza urbana de Fortaleza. Para o gerenciamento dos jobs de ETL utiliza-se a ferramenta Prefect, garantindo confiabilidade e monitoramento eficiente.

## Estrutura do Repositório

- **scripts/**
  - Esta pasta contém os scripts de ETL para diferentes sistemas.
- **docs/**
  - Documentação sobre o projeto e como utilizar os scripts de ETL.
- **config/**
  - Configurações necessárias para cada sistema e para o pipeline em geral.
- **samples/**
  - Amostras de dados de entrada e saída para teste e validação.
- **utils/**
  - Utilitários e ferramentas auxiliares para o pipeline.

## Como Usar

1. **Configuração**: Antes de executar os scripts de ETL, certifique-se de configurar corretamente as informações de conexão com os sistemas fonte e destino no diretório `config/`.
2. **Execução dos Scripts**: Navegue até o diretório `scripts/` e execute os scripts de ETL correspondentes aos sistemas dos quais deseja extrair dados.
3. **Monitoramento e Logs**: Os logs de execução estarão disponíveis no diretório `logs/`, onde você poderá monitorar o progresso e quaisquer erros que possam ocorrer durante o processo de ETL.
4. **Validação dos Dados**: Após a execução bem-sucedida dos scripts, verifique os dados resultantes nos diretórios de saída especificados nos scripts ou na pasta `samples/`.
5. **Manutenção e Atualização**: Periodicamente, verifique se há atualizações nos sistemas fonte e adapte os scripts de ETL conforme necessário para garantir a consistência e a integridade dos dados.

## Contribuições

Contribuições são bem-vindas! Se você tem ideias para melhorar este pipeline, sinta-se à vontade para abrir uma issue ou enviar um pull request.

## Licença

Este projeto é licenciado sob a [MIT License](LICENSE).

## Contato

Para perguntas, sugestões ou suporte, entre em contato através dos issues deste repositório ou envie um e-mail para [pedro.almeida@iplanfor.fortaleza.ce.gov.br].